# htbrown/dotfiles
Configuration files for basically everything I can be bothered to push to git.
Used on macOS and Linux (WSL), so there's a couple things which involve checks to make sure things 
work nicely.

## Installation
If, for some reason, you would like to use my configuration files, you have two options: copy things across manually or use [GNU stow](https://www.gnu.org/software/stow/stow.html).
Note that using stow (or the install script within this repo) will create symbolic links back to this folder.
Make sure you are happy with where you clone this before you continue.

**BEFORE YOU CONTINUE:** Backup your own configuration files. These scripts shouldn't overwrite anything, but it's probably a good idea anyway.

### Method 1: Using GNU stow

#### 0. Install GNU stow

Follow the [instructions](https://www.gnu.org/software/stow/stow.html) to install stow on your system.

#### 1. Run the install script

You can now run `stow.sh` to "stow" all the configuration files within this folder. It'll show you if any conflicts occur.

#### Alternatively

You can use the `stow` command manually to figure out what you want to install yourself.

### Method 2: Copy/Paste

Fairly self-explanatory tbh.

## Contributions
Or perhaps more criticisms. I have no idea what I'm doing, so if you see something that you feel is
absolutely abhorrent (particularly in my neovim configuration), feel free to criticise me. I do ask
that your criticism be constructive, however, and not based solely on personal preference. These are
my dotfiles, after all.
