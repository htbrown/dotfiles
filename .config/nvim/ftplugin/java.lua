local home = os.getenv("HOME")
local project_name = vim.fn.fnamemodify(vim.fn.getcwd(), ":p:h:t")
local project_dir = vim.fn.getcwd()
local workspace_dir = home .. "/.jdtls/cache/workspace/" .. project_name
local jdtls_bin = vim.fn.stdpath("data") .. "/mason/bin/jdtls"

local get_libraries = function()
    local res = {}
    local file = io.open(project_dir .. "/.libraries.jdtls", "rb");
    if file then
        file:close()
        for line in io.lines(project_dir .. "/.libraries.jdtls") do
            res[#res+1] = line
        end
    end
    return res
end

local config = {
    -- cmd to start language server
    -- see: https://github.com/eclipse/eclipse.jdt.ls#running-from-the-command-line
    cmd = {
        -- jdtls install location
        jdtls_bin,
        -- data folder directory, see relevant section in readme
        '-data', workspace_dir
    },
    -- default root dir if not provided
    root_dir = vim.fs.root(0, {".git", "mvnw", "gradlew", ".libraries.jdtls"}),
    settings = {
        java = {
            project = {
                referencedLibraries = get_libraries()
            }
        },
    }
}

-- start lsp
require('jdtls').start_or_attach(config)
