vim.opt_local.wrap = true
vim.opt_local.linebreak = true

vim.keymap.set("n", "<leader>c", function() vim.cmd("VimtexCompile") end)
