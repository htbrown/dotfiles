-- line numbers
vim.opt.nu = true
vim.opt.relativenumber = false

-- indentation
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

-- line wrap
vim.opt.wrap = false

-- 24 bit colour
vim.opt.termguicolors = true

-- use system clipboard
if vim.fn.has("unnamedplus") == 1 then
    vim.opt.clipboard = "unnamedplus"
else
    vim.opt.clipboard = "unnamed"
end

-- emojis
vim.cmd("set noemoji")
