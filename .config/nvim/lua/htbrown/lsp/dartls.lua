return {
    enabled = true,
    mason = false,
    init = function()
        require("lspconfig").dartls.setup({
            cmd = { "dart", "language-server", "--protocol=lsp" }
        })
    end
}
