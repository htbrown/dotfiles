return {
    enabled = true,
    init = function()
        require("lspconfig").eslint.setup({
            on_attach = function(_, bufnr)  -- client, bufnr
                vim.api.nvim_create_autocmd("BufWritePre", {
                    buffer = bufnr,
                    command = "EslintFixAll",
                })
            end,
        })
    end
}
