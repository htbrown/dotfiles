return {
    enabled = true,
    init = function()
        require("lspconfig").pylsp.setup({
            settings = {
                pylsp = {
                    plugins = {
                        pycodestyle = {
                            maxLineLength = 128,
                            indentSize = 4
                        }
                    }
                }
            }
        })
    end
}
