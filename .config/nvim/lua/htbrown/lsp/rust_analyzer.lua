return {
    enabled = false,
    init = function()
        local capabilities = vim.lsp.protocol.make_client_capabilities()

        require("lspconfig").rust_analyzer.setup({
            capabilities = capabilities,
            settings = {
                ['rust-analyzer'] = {
                    diagnostics = {
                        enable = true
                    },
                    cargo = {
                        allFeatures = true
                    }
                }
            }
        })
    end
}
