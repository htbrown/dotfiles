return {
    enabled = false,  -- using nvim-jdtls instead
    mason = true,  -- but make sure mason installs jdtls
    init = function()
        require("lspconfig").jdtls.setup({
            single_file_support = true,
            root_dir = vim.fs.dirname(vim.fs.find({'gradlew', '.git', 'mvnw'}, { upward = true })[1]),
        })
    end
}
