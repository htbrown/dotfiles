local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	spec = "htbrown.plugins",
    change_detection = { notify = false }
})
require('lazy.view.config').keys.close = '<Esc>'

-- keybinds
vim.keymap.set("n", "<leader>ll", function() vim.cmd("Lazy") end)
vim.keymap.set("n", "<leader>ls", function() vim.cmd("Lazy sync") end)
