vim.g.mapleader = " "

-- terminals
vim.keymap.set("n", "<leader>sh", function() vim.cmd("vsplit") vim.cmd("wincmd l") vim.cmd("terminal") end)
vim.keymap.set("t", "<esc>", "<C-\\><C-n>")

-- split navigation
vim.keymap.set("n", "<C-h>", function() vim.cmd("wincmd h") end)
vim.keymap.set("n", "<C-j>", function() vim.cmd("wincmd j") end)
vim.keymap.set("n", "<C-k>", function() vim.cmd("wincmd k") end)
vim.keymap.set("n", "<C-l>", function() vim.cmd("wincmd l") end)

-- nohl
vim.keymap.set("n", "<leader>h", function() vim.cmd("nohl") end)
