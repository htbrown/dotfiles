return {
    "hrsh7th/nvim-cmp",
    dependencies = { "hrsh7th/cmp-nvim-lsp", "L3MON4D3/LuaSnip" },
    config = function()
        local cmp = require("cmp")
        cmp.setup({
            sources = {
                { name = "nvim_lsp" },
                { name = "neorg" }
            },
            mapping = {
                ['<tab>'] = cmp.mapping.confirm({select = true}),
                ['<C-e>'] = cmp.mapping.abort(),
                ['<C-k>'] = cmp.mapping.select_prev_item({behavior = 'select'}),
                ['<C-j>'] = cmp.mapping.select_next_item({behavior = 'select'}),
            },
            snipping = {
                expand = function(args)
                    require("luasnip").lsp_expand(args.body)
                end
            }
        })
    end
}
