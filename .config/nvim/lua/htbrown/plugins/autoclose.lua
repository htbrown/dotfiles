return {
    "m4xshen/autoclose.nvim",
    config = function()
        require("autoclose").setup({
            options = {
                disabled_filetypes = { "text", "markdown", "tex", "latex", "norg", "html" },
                disable_when_touch = true,
                pair_spaces = true,
                auto_indent = false
            }
        })
    end
}
