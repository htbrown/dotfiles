return {
    "folke/zen-mode.nvim",
    lazy = false,
    opts = {

    },
    keys = {
        { "<leader>z", "<cmd>ZenMode<cr>" },
    }
}
