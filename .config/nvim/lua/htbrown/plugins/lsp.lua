GetLspConfigs = function()
    local lsp_zero = require("lsp-zero")
    local ensure_installed = {}
    local handlers = {
        lsp_zero.defualt_setup,
    }

    for _, v in pairs(vim.api.nvim_get_runtime_file("lua/htbrown/lsp/*.lua", true)) do
        local filename = (v:match("^.+/(.+)$"):gsub(".lua", ""))
        local server = require("htbrown.lsp." .. filename)
        if server.mason ~= false then ensure_installed[#ensure_installed+1] = filename end
        if server.enabled and server.init ~= nil and server.mason ~= false then handlers[filename] = server.init end

        if server.mason == false then server.init() end
    end

    return {
        ensure_installed = ensure_installed,
        handlers = handlers
    }
end

return {
    "VonHeikemen/lsp-zero.nvim",
    dependencies = {
        "neovim/nvim-lspconfig",
        "williamboman/mason-lspconfig.nvim",
        "williamboman/mason.nvim"
    },
    config = function()
        local lsp_zero = require("lsp-zero")
        lsp_zero.on_attach(function(_, bufnr)
            -- see :help lsp-zero-keybindings
            -- to learn the available actions
            lsp_zero.default_keymaps({buffer = bufnr})
        end)

        -- show diagnostics in insert mode
        vim.diagnostic.config({
            update_in_insert = true
        })

        -- lsp servers
        local lsp_handlers = GetLspConfigs()
        require("mason").setup({})
        require("mason-lspconfig").setup({
            ensure_installed = lsp_handlers.ensure_installed,
            handlers = lsp_handlers.handlers,
            automatic_installation = true
        })
    end
}
