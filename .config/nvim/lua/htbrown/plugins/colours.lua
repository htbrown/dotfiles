function SetTheme(theme)
    theme = theme or "tokyonight"
    vim.cmd.colorscheme(theme)
end

return {
    {
        "folke/tokyonight.nvim",
        config = function()
            require("tokyonight").setup({
                transparent = true,
                styles = {
                    floats = "dark",
                    sidebars = "dark"
                }
            })
            SetTheme()
        end
    }
}
