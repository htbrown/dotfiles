return {
    "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
        require("nvim-treesitter.configs").setup({
            ensure_installed = { "c", "lua", "vim", "vimdoc", "query", "elixir", "heex", "javascript", "html", "css", "python", "rust", "astro", "scss", "typescript" },
            sync_install = false,
            auto_install = true,
            highlight = {
                enable = true,
                disable = { "latex", "tex" }
            },
            indent = { enable = true },
        })
    end
}
