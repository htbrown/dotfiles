return {
    "lervag/vimtex",
    init = function()
        vim.g.vimtex_compiler_latexmk = {
            options = {
                "-synctex=1",
                "-interaction=nonstopmode",
                "-file-line-error",
                "-pdf",
                "-outdir=build",
            }
        }
    end
}
