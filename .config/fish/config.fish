# clear last login message
clear

if status is-interactive
    # Commands to run in interactive sessions can go here
end

# environment variables
if test -e /opt/homebrew
    set fish_user_paths /opt/homebrew/bin $fish_user_paths
    set fish_user_paths /opt/homebrew/sbin $fish_user_paths
end
if test -e $HOME/.ghcup
    set fish_user_paths $HOME/.ghcup/bin $fish_user_paths
end
if test -e $HOME/.cargo
    set fish_user_paths $HOME/.cargo/bin $fish_user_paths
end
#set fish_greeting "fish v$FISH_VERSION"
set fish_greeting ""

# aliases
alias py python3
alias pip pip3
alias vim nvim
alias ghc ghc-9.4
alias lg lazygit
alias cowfort "fortune -s -e | cowsay -r -n | lolcat"

# starship init
starship init fish | source
# asdf init
if test -e /opt/homebrew/opt/asdf
    source /opt/homebrew/opt/asdf/libexec/asdf.fish
    source $HOME/.asdf/plugins/java/set-java-home.fish
else if test -e $HOME/.asdf/asdf.fish;
    source $HOME/.asdf/asdf.fish
end
# iterm integration init
test -e {$HOME}/.iterm2_shell_integration.fish ; and source {$HOME}/.iterm2_shell_integration.fish

# and finally, a quote for luck
if type -q fortune && type -q lolcat && type -q cowsay
    cowfort 
end
