local wezterm = require("wezterm")
local config = wezterm.config_builder()

-- tabs
wezterm.use_fancy_tab_bar = false
wezterm.hide_tab_bar_if_only_one_tab = true

-- font
config.font = wezterm.font_with_fallback({
    "JetBrainsMono Nerd Font",
    "Consolas"
})
config.font_size = 14

return config
