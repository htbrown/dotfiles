#!/bin/bash

COLOUR_RED="\033[41m"
COLOUR_YELLOW="\033[33m"
COLOUR_GREEN="\033[42m"
COLOUR_BOLD="\033[1m"
COLOUR_RESET="\033[0m"

#PACKAGE=$1
TARGET_DIR=$2
DOTFILE_DIR=$(pwd)
#if [[ $PACKAGE == "" ]]; then
#    echo -e "${COLOUR_YELLOW}no package given, defaulting to all.${COLOUR_RESET}"
#    PACKAGE="."
#fi
if [[ $TARGET_DIR == "" ]]; then
    echo -e "${COLOUR_YELLOW}no target directory given, defaulting to home.${COLOUR_RESET}"
    TARGET_DIR=$HOME
fi

if [[ ! -d $TARGET_DIR ]]; then
    echo -e "${COLOUR_RED}target directory doesn't exist! aborting...${COLOUR_RESET}"
    return 1 2>/dev/null
    exit "1"
fi

if ! command -v stow 2>&1 >/dev/null; then
    echo -e "${COLOUR_RED}stow not found on system! aborting...${COLOUR_RESET}"
    return 1 2>/dev/null
    exit "1"
fi

echo
echo -e "${COLOUR_BOLD}pre-run${COLOUR_RESET}"
echo
#if [[ $PACKAGE == "." ]]; then
#    echo -e "package is '.' so all files/folders in this directory will be symlinked to target dir (other than in .stow-local-ignore)."
#fi
echo "simulating stow..."
stow -n -t $TARGET_DIR .
echo
echo -e "${COLOUR_BOLD}please check above for errors or warnings (ignore any about simulation).${COLOUR_RESET}"
echo -e "${COLOUR_BOLD}${COLOUR_YELLOW}if stow is shouting about not being able to overwrite existing files, you may have to clean these up yourself. also, BACK UP YOUR OWN CONFIGS BEFORE CONTINUING. thanks :)${COLOUR_RESET}"
#echo -e "F${COLOUR_BOLD}${COLOUR_YELLOW}if you continue, this script will delete all directories listed in scripts/stow-prepare.sh. please check this file and BACK UP YOUR OWN CONFIGURATION before continuing.${COLOUR_RESET}"
read -p "continue? (y/n): " confirm && [[ $confirm == [yY] || $confirm == [yY][eE][sS] ]] || exit 1

echo -e "${COLOUR_BOLD}running stow...${COLOUR_RESET}"
if stow -t $TARGET_DIR .; then
    echo
    echo -e "${COLOUR_GREEN}success!${COLOUR_RESET}"
else
    echo
    echo -e "${COLOUR_RED}an error occurred while running stow, see above. aborting...${COLOUR_RESET}"
fi
